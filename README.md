# restClient



## About

A simple Spring Boot application that uses WebFlux for consuming REST APIs.

## Getting started

Clone repository on your local machine and simply start the application. 

For testing part:

POST: http://localhost:8080/data with a body such as 
```json
{
    "userId": 123,
    "id": 123,
    "title": "my title",
    "body": "my body..."
}
```
GET: http://localhost:8080/data

PUT: http://localhost:8080/data with a body such as 
```json
{
    "userId": 123,
    "id": 123,
    "title": "my title updated",
    "body": "my body updated..."
  }
```
DELETE: http://localhost:8080/data/1
