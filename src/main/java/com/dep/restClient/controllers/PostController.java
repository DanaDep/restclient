package com.dep.restClient.controllers;

import com.dep.restClient.models.Post;
import com.dep.restClient.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/data")
public class PostController {
    @Autowired
    private PostService postService;

    @PostMapping
    public Post createData(@RequestBody Post post){
        return postService.createNewPost(post);

    }
    @GetMapping
    public Post[] findAllData(){
        return postService.findAllPosts();
    }

    @PutMapping
    public Post deleteData(@RequestBody Post post){
        return postService.updatePost(post);
    }

    @DeleteMapping
    public Post deleteData(){
        return postService.deletePost(1);
    }
}
