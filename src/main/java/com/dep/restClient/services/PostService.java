package com.dep.restClient.services;

import com.dep.restClient.models.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class PostService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PostService.class);
    @Autowired
    private WebClient webClient;

    public Post createNewPost(Post post){
        LOGGER.info("Creating a new post");
        return webClient
                .post()
                .uri("/posts")
                .body(Mono.just(post), Post.class)
                .retrieve()
                .onStatus(
                        (HttpStatus::isError),
                        (clientResponse) -> Mono.error(new Throwable("Error on creating flow"))
                )
                .bodyToMono(Post.class)
                .block();
    }

    public Post[] findAllPosts() {
        LOGGER.info("Finding all posts");
        return
                webClient
                .get()
                .uri("/posts")
                .retrieve()
                .onStatus(
                        (HttpStatus::isError),
                        (clientResponse) -> Mono.error(new Throwable("Error on getting flow"))
                )
                .bodyToMono(Post[].class)
                .block();
    }

    public Post updatePost(Post post){
        LOGGER.info("Updating post with id {}", post.getId());
        return webClient
                .put()
                .uri("/posts/" + post.getId())
                .body(Mono.just(post), Post.class)
                .retrieve()
                .onStatus(
                        (HttpStatus::isError),
                        (clientResponse) -> Mono.error(new Throwable("Error on updating flow"))
                )
                .bodyToMono(Post.class)
                .block();
    }

    public Post deletePost(int postId){
        LOGGER.info("Deleting post with id {}", postId);
        return webClient
                .delete()
                .uri("/posts/" + postId)
                .retrieve()
                .onStatus(
                        (HttpStatus::isError),
                        (clientResponse) -> Mono.error(new Throwable("Error on deleting flow"))
                )
                .bodyToMono(Post.class)
                .block();
    }
}
